import React from "react";
import Layout from "./components/Layout";

function App() {
  return (
    <div className="App">
      <Layout title="Chat app" />
    </div>
  );
}

export default App;
