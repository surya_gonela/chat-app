import React, { Component } from "react";
import io from "socket.io-client";

const socketUrl = "http://localhost:3132";

class Layout extends Component {
  state = {
    socket: null
  };

  componentWillMount = () => {
    this.initSocket();
  };

  initSocket = () => {
    const socket = io(socketUrl);
    socket.on("connect", () => {
      console.log("Connected");
    });
    this.setState({
      socket
    });
  };

  render() {
    const { title } = this.props;
    return <div className="container">{title}</div>;
  }
}

export default Layout;
